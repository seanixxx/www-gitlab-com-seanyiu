#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'
require 'yaml'

Dir['data/team_members/person/**/*.yml'].each do |path|
  member = YAML.load_file(path, permitted_classes: [Date])

  department = ['Engineering Function', 'Product'].find do |department|
    member.dig('departments')&.include?(department)
  end

  next unless department

  member_array = member.to_a
  departments_index = member_array.index { |(key, _)| key == 'departments' }
  division = department[/\A\w+/].downcase

  member_array.insert(departments_index, ['division', division])

  File.write(path, YAML.dump(member_array.to_h))
end
